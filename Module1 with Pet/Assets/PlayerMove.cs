﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMove : MonoBehaviour
{
    
    public InputAction movement;
    public CharacterController controller;
    public float movementSpeed;

    void OnEnable(){

        movement.Enable();
    }

    void OnDisable(){

        movement.Disable();
    }

    void Start(){

        controller = GetComponent<CharacterController>();
    }
    void LateUpdate(){

        Vector2 inputVector = movement.ReadValue<Vector2>();
        Vector3 finalVector = new Vector3();
        finalVector.x = inputVector.x;
        finalVector.z = inputVector.y;
        controller.Move(finalVector * Time.deltaTime* movementSpeed);

    }
}
