﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class travelToDrirection : MonoBehaviour
{

    public Vector3 direction = new Vector3(8, 0, -4);
    float movementspeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        
        direction *= .01f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        transform.Translate(direction.normalized * movementspeed * Time.deltaTime);
    }
}
