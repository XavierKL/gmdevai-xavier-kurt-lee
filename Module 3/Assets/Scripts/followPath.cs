﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPath : MonoBehaviour
{

    Transform goal;
    float speed = 4.0f;
    float accuracy = 1.0f;
    float rotSpeed = 5.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWaypointIndex = 0;
    Graph graph;

    // Start is called before the first frame update
    void Start()
    {
        
        wps = wpManager.GetComponent<waypointManaer>().waypoints;
        graph = wpManager.GetComponent<waypointManaer>().graph;
        currentNode = wps[0];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        if(graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength()){

            return;
        }

        //node that is closest

        currentNode = graph.getPathPoint(currentWaypointIndex);

        //if close enough then move to next waypoint

        if(Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.position) < accuracy){

            currentWaypointIndex++;
        }

        //if not at end of path

        if(currentWaypointIndex < graph.getPathLength()){

            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction),
                                                        Time.deltaTime * rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void goToHelipad(){ //runs A* algorithm and auto updates

        graph.AStar(currentNode, wps[0]);
        currentWaypointIndex = 0;
    }

    public void goToTwinMountains(){

        graph.AStar(currentNode, wps[2]);
        currentWaypointIndex = 0;
    }

    public void goToBarracks(){

        graph.AStar(currentNode, wps[3]);
        currentWaypointIndex = 0;
    }

    public void goToMiddle(){

        graph.AStar(currentNode, wps[4]);
        currentWaypointIndex = 0;
    }

    public void goToRuins(){

        graph.AStar(currentNode, wps[6]);
        currentWaypointIndex = 0;
    }

    public void goToFactory(){

        graph.AStar(currentNode, wps[7]);
        currentWaypointIndex = 0;
    }

    public void goToCommandPost(){

        graph.AStar(currentNode, wps[11]);
        currentWaypointIndex = 0;
    }

    public void goToRadar(){

        graph.AStar(currentNode, wps[12]);
        currentWaypointIndex = 0;
    }
    public void goToTankers(){

        graph.AStar(currentNode, wps[13]);
        currentWaypointIndex = 0;
    }


    public void goToCommandCenter(){

        graph.AStar(currentNode, wps[14]);
        currentWaypointIndex = 0;
    }
    
}

