﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AngryGuy : MonoBehaviour
{

    //zombie will start to chase the player down(Agent 1)

    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    Vector3 wanderingTarget;
    float distanceBetweenObjects;

    // Start is called before the first frame update
    void Start()
    {

        agent = this.GetComponent<NavMeshAgent>(); 
        playerMovement = target.GetComponent<WASDMovement>();   
    }

    // Update is called once per frame
    void Update()
    {

        distanceBetweenObjects = Vector3.Distance(target.transform.position, this.transform.position);

        if(distanceBetweenObjects < 10){

            Pursue();
        }

        else {

            Wander();
        }
    }

    void Wander(){

        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderingTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderingTarget.Normalize();
        wanderingTarget *= wanderRadius;

        Vector3 targetLocal = wanderingTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    void Seek(Vector3 location){

        agent.SetDestination(location);
    }

    void Pursue(){

        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude/(agent.speed + playerMovement.currentSpeed);
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }
}
