﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShyGuy : MonoBehaviour
{
    
    // zombie will never meet the player here(Agent 2)

    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    Vector3 wanderingTarget;
    float distanceBetweenObjects;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>(); 
        playerMovement = target.GetComponent<WASDMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
        distanceBetweenObjects = Vector3.Distance(target.transform.position, this.transform.position);

        if(distanceBetweenObjects < 10){

            Hide();
        }

        else {

            Wander();
        }
    }

    void Wander(){

        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderingTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderingTarget.Normalize();
        wanderingTarget *= wanderRadius;

        Vector3 targetLocal = wanderingTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    void Hide(){

        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotsCount = world.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSpotsCount; i++){

            Vector3 hideDirection = world.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = world.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance){

                chosenSpot = hidePosition;
                distance = spotDistance;
            }

            Seek(chosenSpot);
        }
    }

    void Flee(Vector3 location){

        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Seek(Vector3 location){

        agent.SetDestination(location);
    }

}
