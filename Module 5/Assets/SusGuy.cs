﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SusGuy : MonoBehaviour
{

    // zombie will evade player here(Agent 3)

    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    Vector3 wanderingTarget;
    float distanceBetweenObjects;

    // Start is called before the first frame update
    void Start()
    {

        agent = this.GetComponent<NavMeshAgent>(); 
        playerMovement = target.GetComponent<WASDMovement>();   
    }

    // Update is called once per frame
    void Update()
    {
        distanceBetweenObjects = Vector3.Distance(target.transform.position, this.transform.position);

        if(distanceBetweenObjects < 10){

            Evade();
        }

        else {

            Wander();
        }
    }

    void Wander(){

        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderingTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderingTarget.Normalize();
        wanderingTarget *= wanderRadius;

        Vector3 targetLocal = wanderingTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld); 
    }

    void Flee(Vector3 location){

        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Evade(){

        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude/(agent.speed + playerMovement.currentSpeed);
        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    void Seek(Vector3 location){

        agent.SetDestination(location);
    }
}
