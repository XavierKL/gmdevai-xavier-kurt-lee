﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{

    GameObject[] goalLocations;
    NavMeshAgent agent;
    Animator anim;
    float speedMultiplier;
    float detectRadius = 20;
    float fleeRadius;
    // Start is called before the first frame update
    void Start()
    {
        
        goalLocations = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        anim = this.GetComponent<Animator>();

        agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        anim.SetTrigger("isWalking");
        anim.SetFloat("walkingOffset", Random.Range(0.1f, 1f));
        resetAgent();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        if(agent.remainingDistance < 1){
            
            agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        }
    }

    void resetAgent(){

        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        anim.SetFloat("speedMultiplier", speedMultiplier);
        anim.SetTrigger("isWalking");
        agent.ResetPath();
    }

    public void DetectNewObstacle(Vector3 location){

        if(Vector3.Distance(location, this.transform.position) < detectRadius){

            fleeRadius = 10;
            Vector3 fleeDirection = (this.transform.position - location).normalized; //gets the inverse of location
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if(path.status != NavMeshPathStatus.PathInvalid){

                agent.SetDestination(path.corners[path.corners.Length - 1]);
                anim.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }

    public void GoToNewObstacle(Vector3 location){

        if(Vector3.Distance(location, this.transform.position) < detectRadius){

            fleeRadius = 5;
            Vector3 goalDirection = (this.transform.position - location).normalized;
            Vector3 newGoal = this.transform.position - goalDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if(path.status != NavMeshPathStatus.PathInvalid){

                agent.SetDestination(path.corners[path.corners.Length - 1]);
                anim.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }
}
